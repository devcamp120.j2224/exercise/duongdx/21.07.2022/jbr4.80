package com.devcamp.s50.jbr4_80;

public class Square extends shape{
    private double side ;

    public Square(String color, boolean filled, double side) {
        super(color, filled);
        this.side = side;
    }

    public Square(double side) {
        this.side = side;
    }
    
    public Square(){
        
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public double getArea(){
        return side * side ;
    }

    public double getPerimeter(){
        return (side + side) * 2 ;
    }

    @Override
    public String toString() {
        return "Square [shape: color=" + super.getColor() + ", filled=" + super.isFilled() + "], side" + side + "]";
    }
    
}
