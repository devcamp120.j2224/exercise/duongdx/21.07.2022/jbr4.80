package com.devcamp.s50.jbr4_80;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShapeController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getCircleArea(){
        Circle circle = new Circle("yellow", true , 42);

        return circle.getArea();
    };

    @CrossOrigin
    @GetMapping("/circle-perimete")
    public double getCirclePerimete(){
        Circle circle = new Circle("yellow", true , 42);

        return circle.getPerimeter();
    };

    @CrossOrigin
    @GetMapping("/rectangle-area")
    public double getRectangleArea(){
        Rectangle rectangle = new Rectangle("blue", false, 12, 6);

        return rectangle.getArea();
    };

    @CrossOrigin
    @GetMapping("/rectangle-perimete")
    public double getRectanglePerimete(){
        Rectangle rectangle = new Rectangle("red", false, 12, 6);

        return rectangle.getPerimeter();
    };

    @CrossOrigin
    @GetMapping("/Square-area")
    public double getSquareArea(){
        Square square = new Square("brown", true, 25);

        return square.getArea();
    };

    @CrossOrigin
    @GetMapping("/Square-perimete")
    public double getSquarePerimete(){
        Square square = new Square("brown", true, 25);

        return square.getPerimeter();
    };
}
