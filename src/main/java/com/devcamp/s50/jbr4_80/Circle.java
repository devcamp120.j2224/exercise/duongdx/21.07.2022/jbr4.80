package com.devcamp.s50.jbr4_80;

public class Circle extends shape{
    private double radius = 1.0 ;

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle(double radius) {
        this.radius = radius;
    }
    
    public Circle(){
        
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI * radius * radius ;
    }

    public double getPerimeter(){
        return Math.PI * radius * 2 ;
    }

    @Override
    public String toString() {
        return "Circle [shape [color=" + super.getColor() + ", filled=" + super.isFilled() + "], radius=" + radius + "]";
    }
}
